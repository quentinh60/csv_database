"""
Authors: MONT Louis
         MONTOUCHET Teddy
"""
import csv
import os
import sqlite3
import datetime
import logging


def getCSV(file, delimiter):
    """    
    Décrypte le CSV

    Parameters:
        file: le fichier à lire
        delimiter:le délimiteur entre les données

    Returns:
        Retourne la liste de listes composée de tout les éléments du CSV
    """
    thisdelimiter = delimiter
    if(os.path.exists(file)):
        if(os.path.isfile(file)):
            with open(file) as csvfile:
                reader = csv.reader(csvfile, delimiter=thisdelimiter)
                logging.info("File opened")
                dataRows = []
                for row in reader:
                    dataRows.append(row)
                return dataRows
        else:
            logging.warning("Path %s do not exists" % file)
    else:
        logging.warning("File %s is not of expected type" % file)


def modifyRows(dataRows, sqlFile):
    """
    Modifie les lignes de la base de données pour rajouter ou modifier celle correspondante

    Parameters:
        dataRows: La liste contenant toutes les lignes du .csv
        sqlFile: Le fichier SQL dans lequel on lit/écrit

    """
    connection = sqlite3.connect(sqlFile)
    logging.debug("Connected to file")
    cursor = connection.cursor()
    attributes = dataRows.pop(0)
    for row in dataRows:
        immatriculation= row[attributes.index("immatriculation")]        
        cursor.execute('''SELECT * FROM SIV WHERE immatriculation=?;''',(immatriculation,))
        if(cursor.fetchall() == []):
            logging.debug("Row %s not found, creating..." % immatriculation)
        else:
            logging.debug("Row %s found, modifying..." % immatriculation)
        columns = ""
        values = ""
        for attr in row:
            columns = (('? ?,'),(columns, attributes[row.index(attr)]))
            values = (('? ?,'),(values, attr))
        columns = columns[:-1]
        values = values[:-1]
        command = str.format(
            'INSERT INTO SIV (%s) VALUES (%s);' % (columns, values))
        logging.debug("Row %s created/modified" % immatriculation)
        cursor.execute(command)
    connection.commit()
    logging.debug("Commands committed")
    cursor.close()
    connection.close()


def verifBDDAndSIV(attributeRow, sqlFile):
    """
    Vérifie si la BDD et/ou la table est créée

    Parameters:
        attributeRow: la ligne contenant tout les attributs dans le CSV
        sqlFile: le fichier dans lequel est enregistré la base de données
    """
    if(os.path.exists(sqlFile)):
        logging.debug("File does not exist, creating...")
    connection = sqlite3.connect(sqlFile)
    logging.debug("Connected to file")
    cursor = connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    if(cursor.fetchall() != []):
        logging.debug("No table existing, creating...")
    else:
        command = ''
        for attribute in attributeRow:
            command += str.format(',%s TEXT' % attribute)
        command = command[1:]
        cursor.execute(
            str.format('CREATE TABLE SIV(%s);' % command))
        logging.debug("Table created")
    connection.commit()
    logging.debug("Commands committed")
    cursor.close()
    connection.close()


def main(file, delimiter, sqlFile):
    """
    Main

    Parameters:
        file: le fichier à lire
        delimiter:le délimiteur entre les données
        sqlFile: le fichier dans lequel est enregistré la base de données

    """
    logging.basicConfig(filename='CSVSQL.log',
                        level=logging.DEBUG, format='%(asctime)s: %(message)s')
    dataRows = getCSV(file, delimiter)
    logging.info("CSV get in list of lists")
    verifBDDAndSIV(dataRows[0], sqlFile)
    logging.info("Verified DB and Table created if not exist them")
    modifyRows(dataRows, sqlFile)
    logging.info("DB Updated")


if __name__ == "__main__":
    main("newauto.csv", ';', 'BDDCSV.sqlite3')
